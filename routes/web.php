<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/', function(){
    return '<h1 style="text-align: center">Hello Developer! I am alive :)</h1>';
});*/
Route::view('/', 'main');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

/**
 * Dashboard routes
 */
Route::group([
    'prefix' => 'dashboard',
    'as' => 'dashboard::',
    'middleware' => 'auth'
], function () {

    Route::get('/', [
        'as' => 'index',
        'uses' => function () {
            return app()->make(\Webmagic\Dashboard\Dashboard::class)->render();
        }
    ]);
});
