#!/usr/bin/env bash

# CRON
service cron start

# Supervisor
service supervisor start

# To have no issues with permissions
chown -R www-data:www-data /srv/app/storage
chown -R www-data:www-data /srv/app/bootstrap/cache

# Apache
apache2-foreground
