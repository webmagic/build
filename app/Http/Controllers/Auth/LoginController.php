<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Webmagic\Dashboard\Components\FormGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Pages\LoginPage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return LoginPage
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function showLoginForm()
    {
        return (new LoginPage($this->loginForm()));
    }

    /**
     * @return \Webmagic\Dashboard\Pages\LoginPage
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function loginForm()
    {
        $page = (new \Webmagic\Dashboard\Pages\LoginPage())->setDefaultForm();

        return $page;
    }
}
