### About project

Write here important information about project. And get respect++, and improve your karma :)

# About build

## Fast start

### Run Laravel inside docker container

#### Run minimal docker container
Docker and Docker compose should be installed

Minimal docker container based on [webmagicagency/laravel6.x_php7.3_apache](webmagicagency/laravel6.x_php7.3_apache)

Call `docker-compose up -d` from CLI to start basic minimal container. Additionally MySQL container will be started

### Run docker container with Node.js

Node.js may be required inside the container for using front-end building system. For getting this For getting this [webmagicagency/laravel6.x_php7.3_apache_node12](webmagicagency/laravel6.x_php7.3_apache_node12) image can be used

For using this image the next steps should be done in docker-compose.yml:
* Comment line `image: webmagicagency/laravel6.x_php7.3_apache` 
* Uncomment line `image: webmagicagency/laravel6.x_php7.3_apache_node12`

After that you can call `docker-compose up -d` from CLI

#### Initialize Laravel inside the container
* Call `docker ps` and find your {container name} 
* Go inside the container with the next command `docker exec -it {container_name} /bin/bash`
* Call `composer install` inside the container
* Copy file `.env.exemple` to root directory with name `.env`
* Call `php artisan key:generate` to set security key
* Call in cli `php artisan migrate --seed`
* Open `localhost:8080` to see your app

### Run Laravel without docker container
* Clone this repository to any directory
* Go to root directory of project and call with cli and call `composer install` (PHP 7.3 and composer should be installed before)
* Define directory `public` as root for your app (defined on server level)
* Create DB and user with sufficient rights for it (MySQL 5.7 or other should be installed before)
* Copy file `.env.exemple` to root directory with name `.env`
* Define in `.env` DB, DB user and password
* Call `php artisan key:generate` to set security key
* Call in cli `php artisan migrate --seed`

## Features
Built with [Laravel 6](https://laravel.com/docs/6.x)

#### Test server closed with auth middleware
For simplify organization of closing test server was added additional param to .env ```APP_GLOBAL_AUTH```
It set ```true``` for default. If you need to remove auth middleware for public routes just set ```true``` for .env ```APP_GLOBAL_AUTH```

#### Manual setting up https for urls generation
For the situation when used proxy-pass without https or when https was not recognized automatically by the framework 
app/Providers/RouteServiceProvider.php:27-29

#### .htaccess features
* added compressing
* added redirection for url with ```index.php```

#### Defined identifier for current page
In all views defined variable named `$body_class` in which defined identifier for current page
If you want to remove it, remove `$this->registerServices();` form `\App\Providers\RouteServiceProvider->register()`

#### DB
* Was off strict mode in work with DB for work with groupBy with easy way
* Engine set to **InnoDB**, cause some times on create BD on not wps server, engine was **MyISAM** and cascade not work!    

#### Automation set local
In main template param lang sets automatically based on local which was set in app 

### Testing
The Laravel Dusk installed. The additional options added for capability ``tests/DuskTestCase.php:32``

### Docker using
The docker configuration are included to the build. You have possibility to run 2 different configurations. The first one is prepared for back-end works only. It is set as default. You need just call ``sudo docker-compose up`` to run it.

The second one is prepared for front-end and back-end works. You need to comment this line  `image: webmagicagency/laravel6.x_php7.3_apache` and uncomment line `image: webmagicagency/laravel6.x_php7.3_apache_node12`to use it.  Call ``sudo docker-compose up`` after this to start the docker containers

You can get additional details about working with docker here - [Ubuntu preparing](https://docs.google.com/document/d/16Gsv4XpJ4t8Hz0t2Dw_1raqgtPhnFp7GUwf1ickqkIY/edit?usp=sharing)

#### Docker image customizing
Docker image customizing may be done with using .docker/Dockerfile. For using this file for build the container update the docker-compose.yml:

* comment `image: ...` line 
* uncomment the next lines
```
    build:
      context: .
      dockerfile: .docker/Dockerfile 
```
* modify .docker/Dockerfile as you needed

You can also update `./docker/vhost.conf` to apply additional Appache2 configuration for the host

#### Initialize with start.sh script
Basic services automatically starting configure together with container. It is done by rub Bash script `./start.sh`. 

You can modify this script based on your needs. It will be run automatically with container start.

#### Selenium server using
You can use Selenium server for run browser tests with docker. For doing this you should do the next steps:
* Set option ``USE_SELENIUM=true`` in ``.env`` file
* Uncomment lines connected to Selenium in docker-compose file ``docker-compose.yml:34``
* Set ``APP_URL=http://slenium-app`` in ``.env``

#### Installed WebMagic modules
* [Dashboard](https://bitbucket.org/webmagic/dashboard)
* [Users](https://bitbucket.org/webmagic/users)
* [Request](https://bitbucket.org/webmagic/request)
* [Notifier](https://bitbucket.org/webmagic/mailer)

#### Installed third-party packages
All these packages installed as `require-dev` and will not be installed when call `composer install --no-dev` 

##### Any environments
* [Collition](https://github.com/nunomaduro/collision) - clear console errors

##### Local and dev-server environments
In ```local``` and ```dev-server``` environments will be available:

* [Iseed](https://github.com/orangehill/iseed) - seeders generator
* [MigrationsGenerator](https://github.com/Xethron/migrations-generator) - migrations generator

##### Only local environment
In ```local``` environment only will be available:

* [Debugbar](https://github.com/barryvdh/laravel-debugbar) - debugbar
* [Laravel Dusk](https://laravel.com/docs/5.5/dusk) - for run browser tests

##### Changing environment
For change environment define in `.env` variable `APP_ENV` with name of needed environment. For example `APP_ENV=local`

#### Take in mind when working
* [Rotes structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2756984/Laravel+5.1+-+5.5)
* [Links structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2787862/Laravel)
* [Base structure convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/3022909/Laravel+5.4)
* [Git working convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/40960020/2.+Git)

## Front

#### Fast start
* Check that you using front-end  configuration of docker
* For installed third-party packages run `npm i` in direction  `/resources/assets/_gulp-builder`
* Define root directory in  `gulpfile.js` and `webpack.config.js` 
* Call in _gulp-builder  `gulp watch` or `gulp build`

As a result you will get two js files, libs.js and script.js  and two css files base-styles.css and style.css in root direction in the corresponding folders

#### More information
* [Working with front end build system](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/41386077/4.+Gulp)
* [JS code style convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/2363581/JavaScript+Code+Styling)
* [CSS code style convention](https://wm-dev.atlassian.net/wiki/spaces/WEB/pages/62324949/WebMagic+CSS+code+convention)
